import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {BeersComponent} from './components/beers/beers.component';
import {Routes} from '@angular/router';

export const appRoutes: Routes = [
    {
        path: 'beers',
        component: BeersComponent
    },
    {
        path: '',
        redirectTo: '/beers',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];
