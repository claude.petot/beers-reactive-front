import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/takeWhile';
import {environment} from '../../environments/environment';
import {Beer} from '../common/model/beer';
import {EventsourceService} from '../common/services/eventsource.service';

@Injectable()
export class BeerService {

    private url = environment.rootUrl;
    private beerMapper = data => new Beer(JSON.parse(data));

    constructor(private eventSourceService: EventsourceService) {
    }

    createBeersStream(nbBeersToCreate: number): Observable<Beer[]> {
        return this.eventSourceService.toArrayObservable(`${this.url}/external/beers?nbBeers=${nbBeersToCreate}`, this.beerMapper);
    }

    getBeerByAbvStream(abv: number): Observable<Beer[]> {
        return this.eventSourceService.toArrayObservable(`${this.url}/api/beers?abv=${abv}`, this.beerMapper);
    }

}
