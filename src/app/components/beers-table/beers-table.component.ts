import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs';
import {Beer} from '../../common/model/beer';
import {map, share, startWith} from 'rxjs/operators';

@Component({
    selector: 'app-beers-table',
    templateUrl: './beers-table.component.html',
    styleUrls: ['./beers-table.component.css']
})
export class BeersTableComponent {
    displayedColumns: string[] = ['name', 'degrees', 'country', 'brewery'];

    private _beer$: Observable<Beer[]>;
    nbBeer$: Observable<number>;
    nbCountrie$: Observable<number>;
    nbBrewerie$: Observable<number>;

    get beers() {
        return this._beer$
    };

    @Input()
    set beers(beers: Observable<Beer[]>) {
        this._beer$ = beers.pipe(
            share()
        );
        this.nbBeer$ = this._beer$.pipe(
            map(beers => beers.length),
            startWith(0)
        )
        this.nbCountrie$ = this._beer$.pipe(
            map(beers => beers
                .map(b => b.country ? b.country.title : '')
                .filter(value => !!value)
            ),
            map(countries => new Set(countries).size),
            startWith(0)
        )
        this.nbBrewerie$ = this._beer$.pipe(
            map(beers => beers
                .map(b => b.brewery ? b.brewery.title : '')
                .filter(value => !!value)
            ),
            map(breweries => new Set(breweries).size),
            startWith(0)
        )
    }

    constructor() {
    }

}
