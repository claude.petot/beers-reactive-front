import {Component} from '@angular/core';
import {Beer} from '../../common/model/beer';
import {BeerService} from '../../services/beers.service';
import {Observable} from 'rxjs';
import {share} from 'rxjs/operators';

@Component({
    selector: 'app-beers-search',
    templateUrl: './beers-search.component.html',
    styleUrls: ['./beers-search.component.css']
})
export class BeersSearchComponent {

    searchAbv: number;
    beer$: Observable<Beer[]>;

    constructor(private beerService: BeerService) {
    }

    searchBeers() {
        this.beer$ = this.beerService.getBeerByAbvStream(this.searchAbv).pipe(share());
    }

}
