import {Component} from '@angular/core';
import {BeerService} from '../../services/beers.service';
import {filter, map, share, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, noop, Observable} from 'rxjs';
import {Beer} from '../../common/model/beer';
import {MatSnackBar} from '@angular/material';

@Component({
    selector: 'app-beers-import',
    templateUrl: './beers-import.component.html',
    styleUrls: ['./beers-import.component.css']
})
export class BeersImportComponent {
    nbBeersToCreate = 3;
    beer$: Observable<Beer[]>;
    percentage$: Observable<number>;
    creatingBeers: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(private beerService: BeerService,
                private snackBar: MatSnackBar) {
    }

    createBeers() {
        this.creatingBeers.next(true);
        this.beer$ = this.beerService.createBeersStream(this.nbBeersToCreate).pipe(
            takeUntil(this.creatingBeers.pipe(
                filter(value => !value) // On crée les bières jusqu'à recevoir une falsy value
            )),
            // Nécessaire car sinon le second subscribe en dessous invoque en fait 2 fois le service (à montrer)
            share()
        );

        this.beer$.subscribe(
            noop,
            () => this.snackBar.open('Impossible de récupérer des bières'),
            () => this.creatingBeers.next(false)
        )

        this.percentage$ = this.beer$.pipe(
            map(beers => 100 * (1 - (this.nbBeersToCreate - beers.length) / this.nbBeersToCreate))
        );
    }

    stopCreatingBeers() {
        this.creatingBeers.next(false);
        this.snackBar.open('Import des bières arrêté');
    }
}
