import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeersImportComponent } from './beers-import.component';

describe('BeersImportComponent', () => {
  let component: BeersImportComponent;
  let fixture: ComponentFixture<BeersImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeersImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeersImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
