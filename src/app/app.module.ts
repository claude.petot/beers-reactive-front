import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatProgressBarModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatTableModule,
    MatToolbarModule,
    MatTreeModule
} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {appRoutes} from './app-routes';
import {BeersComponent} from './components/beers/beers.component';
import {BeerService} from './services/beers.service';
import {EventsourceService} from './common/services/eventsource.service';
import {BeersTableComponent} from './components/beers-table/beers-table.component';
import {BeersImportComponent} from './components/beers-import/beers-import.component';
import {BeersSearchComponent} from './components/beers-search/beers-search.component';

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        BeersComponent,
        BeersTableComponent,
        BeersImportComponent,
        BeersSearchComponent
    ],
    imports: [
        // Standards Angular
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        RouterModule.forRoot(appRoutes, {enableTracing: false}),

        // Material
        MatTableModule,
        MatButtonModule,
        MatInputModule,
        MatCardModule,
        MatGridListModule,
        MatSidenavModule,
        MatToolbarModule,
        MatTreeModule,
        MatIconModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatProgressBarModule
    ],
    providers: [BeerService, EventsourceService],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
