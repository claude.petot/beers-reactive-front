import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {EventSourcePolyfill} from 'ng-event-source';

@Injectable()
export class EventsourceService {

    constructor() {
    }

    toObservable<T>(sourceUrl: string, mapper: ((data) => T)): Observable<T> {
        // La donnée mappée est directement celle qui est poussée dans l'observable
        return this.wrapEventSource(sourceUrl, mapper, data => data);
    }

    toArrayObservable<T>(sourceUrl: string, mapper: ((data) => T)): Observable<T[]> {
        // A la réception d'une donnée, on met à jour le tableau et on émet le tableau mis à jour
        const datas: T[] = [];
        return this.wrapEventSource(sourceUrl, mapper, (data: T): T[] => {
            datas.push(data);
            return datas;
        });
    }

    private wrapEventSource<T, R>(sourceUrl: string, mapper: ((data) => T), toObserverMapper: (data: T) => R): Observable<R> {
        return Observable.create((observer) => {
            const eventSource = new EventSourcePolyfill(sourceUrl, {});

            eventSource.onmessage = (event) => {
                console.log('eventSource.onmessage: ', event);
                const data = mapper(event.data);
                if (environment.debug) {
                    console.log('eventSource.onmessage: mapped data : ', data);
                }
                observer.next(toObserverMapper(data));
            };

            // eventSource.onerror = () => observer.error('eventSource.end');
            // Contre-intuitivement, le complete() doit être branché sur le onError de l'eventSource
            eventSource.onerror = () => observer.complete();

            return () => eventSource.close();
        });
    }

}
