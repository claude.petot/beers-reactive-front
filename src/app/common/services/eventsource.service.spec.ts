import { TestBed, inject } from '@angular/core/testing';

import { EventsourceService } from './eventsource.service';

describe('EventsourceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventsourceService]
    });
  });

  it('should be created', inject([EventsourceService], (service: EventsourceService) => {
    expect(service).toBeTruthy();
  }));
});
