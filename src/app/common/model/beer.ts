export class Beer {
    title: string;
    synonyms: string;
    tags: string[];
    abv: number;
    country: {
        title: string
    };
    brewery: {
        title: string
    };

    constructor(jsonData: any) {
        Object.assign(this, jsonData);
    }
}
