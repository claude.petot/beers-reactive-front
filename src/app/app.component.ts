import {Component} from '@angular/core';

@Component({
  selector: 'axl-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
